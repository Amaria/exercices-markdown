
# H1
---
## H2
---
### H3

#### H4

##### H5

###### H6


# Installation du projet 
---
##### Ceci est un projet  ~~pas très important~~  super important 

`npm install`
`composer install`

La doc se trouve dans [Google Drive](https://drive.google.com/)

# Liste de courses
---
Liste des courses:
  * Des carottes
  * Des tomates
  * Un peu de sucre en poudre
  
`<?php echo 'ne pas oublier le sel'; ?>`

# Une image pour le fun 
---
_**ceci est un texte en italique**_

![Texte alternatif](https://scontent-cdg2-1.xx.fbcdn.net/v/t31.0-8/26840703_792663727579821_2787273164296479014_o.jpg?oh=33f2078272101ecf71cbc964f0c5ec24&oe=5B372695)

# Et pourquoi pas un petit tableau ?

| J'aime | J'aime pas            | 
| ------------- |:----------------:|
| le chèvre| x |
| x| Les choux de Bruxelle          |   

Un autre type de tableau ...

```sh
$ npm install --production
$ NODE_ENV=production node app
```

---

_**Comme dirait un apprenant de la promo 4**_
> On a bouffé de la div

___

Liens utilisés pour cette exo : 

_**https://fr.wikipedia.org/wiki/Markdown**_
_**https://github.com/jamstatic/jamstatic-fr/wiki/Pense-b%C3%AAte-Markdown**_

